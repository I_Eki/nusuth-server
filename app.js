const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const serve = require('http').Server(app);
const io = require('socket.io')(serve);

app.use(bodyParser.json({ extended: true }));

app.use(function(req, res, next){
  res.header('Access-Control-Allow-Origin','*');
  next();
});

app.post('/reload', (req, res) => {
  io.emit('reload', req.body);
  res.send();
});

io.on('connection', socket => {
  socket.emit('msg', 'Hello, Nusuth!');
});

const port = 4000;

serve.listen(port, () => {
  console.log(`服务已启动，监听端口${port} . . `);
});
